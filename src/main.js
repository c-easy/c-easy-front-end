import Vue from 'vue';
import App from './App.vue';
import router from './router';
import moment from 'moment';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Vuex from 'vuex';
import Buefy from 'buefy';

import API from './services/api'

library.add(fas,far,fab);
Vue.component('vue-fontawesome', FontAwesomeIcon);

Vue.use(Vuex);
Vue.use(Buefy,  {
    defaultIconComponent: 'vue-fontawesome',
    defaultIconPack: 'fas',
});

Vue.config.productionTip = false;

Vue.filter('formatDate', function(value) {
    if (value) {
        let userLang = navigator.language || navigator.userLanguage;
        moment.locale(userLang);
        return moment(String(value)).format('LLLL');
    }
})

const http = API.initializeAxios();
Vue.prototype.$http = http;

Vue.prototype.moment = moment;

new Vue({
    router,
  render: h => h(App),
}).$mount('#app')
