import Vue from 'vue'
import Vuex from 'vuex'
import chapters from './modules/chapters'
import dos from './modules/dos'
import user from './modules/user'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    chapters,
    user,
    dos
  },
  strict: debug
})