import ChapterAPI from '@/services/chapters'
import CourseAPI from '@/services/courses'
import ExerciseAPI from '@/services/exercises'

const state = {
    allChapters: [],
    allExercises: []
}

const getters = {
    getChapterById: (state) => (id) => {
        return state.allChapters.find(chapter => chapter.id == id);
    },
    getExerciseById: (state) => (id) => {
        return state.allExercises.find(exercise => exercise.id == id);
    }
};

const actions = {
    getAllChapters ({commit}){
        ChapterAPI.getAllChapters().then(result => {
            if(result.success){
                commit('setChapters', result.chapters);
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },
    addChapter({commit}, chapter){
        ChapterAPI.addChapter(chapter).then(result => {
            if(result.success){
                commit('addChapter', result.chapter);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },
    updateChapter({commit}, chapter){
        ChapterAPI.updateChapter(chapter).then(result => {
            if(result.success){
                return result;
            } else {
                commit('updateChapter', result.chapter);
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },
    deleteChapter({commit}, chapter){
        ChapterAPI.deleteChapter(chapter).then(result => {
            if(result.success){
                console.log(result);
                commit('removeChapter', chapter);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error.error}`);
        })
    },

    addCourse({commit}, payload){
        CourseAPI.addCourse(payload.course, payload.file).then(result => {
            if(result.success){
                commit('addCourse', result.course);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },

    updateCourse({commit}, payload){
        CourseAPI.updateCourse(payload.course, payload.hasNewContent, payload.file).then(result => {
            if(result.success){
                commit('updateCourse', result.course);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },

    deleteCourse({commit}, course){
        CourseAPI.deleteCourse(course).then(result => {
            if(result.success){
                commit('removeCourse', course);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error.error}`);
        })
    },

    deleteExercise({commit}, exercise){
        ExerciseAPI.deleteExercise(exercise).then(result => {
            if(result.success){
                commit('removeExercise', exercise);
                return result;
            }
        }).catch(error => {
            throw new Error(`API ${error.error}`);
        })
    },
    
    getAllExercises ({commit}){
        ExerciseAPI.getAllExercises().then(result => {
            if(result.success){
                commit('setExercises', result.exercises);
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    }
}

const mutations = {
    setChapters (state, chapters){
        state.allChapters = chapters;
    },

    addChapter (state, chapter){
        state.allChapters.push(chapter);
    },

    updateChapter (state, chapter){
        let index = state.allChapters.findIndex(element => element.id == chapter.id);
        state.allChapters[index] = chapter;
    },

    removeChapter (state, chapter){
        state.allChapters.splice(chapter, 1);
    },

    addCourse (state, course){
        let index = state.allChapters.findIndex(element => element.id == course.idChapter);
        state.allChapters[index].courses.push(course);
    },

    updateCourse (state, course){
        let index = state.allChapters.findIndex(element => element.id == course.idChapter);
        let indexCourse = state.allChapters[index].courses.findIndex(element => element.id == course.id);
        state.allChapters[index].courses[indexCourse] = course;
    },

    removeCourse (state, course){
        let index = state.allChapters.findIndex(element => element.id == course.idChapter);
        state.allChapters[index].courses.splice(course, 1);
    },

    removeExercise (state, exercise){
        let allChapters = state.allChapters;
        allChapters.forEach(chapter => {
            let index = chapter.courses.findIndex(element => element.id == exercise.idCourse);
            // If index > -1, then we found the exercise !
            if(index != -1){
                chapter.courses[index].exercises.splice(exercise, 1)
            }
        });
        state.allChapters = allChapters;
    },
    setExercises (state, exercises){
        state.allExercises = exercises;
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}