import {getAllDos, addDo} from '@/services/do'


const state = {
    allDos: []
}

const getters = {
    getDoById: (state) => (id) => {
        return state.allDos.find(d => d.idExercise == id);
    }
};

const actions = {
    getAllDos ({commit}){
        getAllDos().then(result => {
            if(result.success){
                commit('setDos', result.dos);
            }            
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },

    addDo ({commit}, exercise){
        return addDo(exercise).then(result => {
            if(result.success){
                commit('addDo', result.do);
                return result.do;
            }
        }).catch(error => {
            throw new Error(`API ${error}`);
        });
    },

    updateDo({commit}, d){
        return new Promise(resolve => {
            commit('updateDo', d);
            resolve(d);
        });
    }
}

const mutations = {
    setDos (state, dos){
        state.allDos = dos;
    },

    addDo (state, d){
        state.allDos.push(d);
    },

    updateDo (state, d){
        let index = state.allDos.findIndex(element => element.idExercise == d.idExercise);
        state.allDos[index] = d;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}