import Vue from 'vue'
import API from '@/services/api'

const state = {
    status: '',
    token: sessionStorage.getItem('ceasyToken') || '',
    user: JSON.parse(sessionStorage.getItem('ceasyUser')) || {}
}

const getters = {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    info: state => state.user,
    isAdmin: state => state.user.type
};

const actions = {
    login ({commit}, data){
        commit('log_request');
        return Vue.prototype.$http.post('user/login', {id: data.user.id, password: data.user.password}).then(result => {
            if(result.data.success){
                sessionStorage.setItem('ceasyToken', result.data.token);
                sessionStorage.setItem('ceasyUser', JSON.stringify(result.data.user));
                API.setAuthorizationHeaders(result.data.token);
                commit('log_success', result.data);
            }
        }).catch(error => {
            commit('log_error');
            throw new Error(`API ${error}`);
        });
    },
    logout ({commit}){
        return new Promise((resolve) => {
            commit('logout');
            sessionStorage.clear();
            API.deleteAuthorizationHeaders();
            resolve();
        });
    }
}

const mutations = {
    log_request (state){
        state.status = 'loading';
    },

    log_success (state, data){
        state.status = 'success';
        state.token = data.token;
        state.user = data.user;
    },

    log_error (state) {
        state.status = 'error';
    },

    logout (state){
        state.status = '';
        state.token = '';
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}