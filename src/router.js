import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

import pageNotFound from './components/pages/PageNotFound'
import home from './components/HomeRoute'
import exercise from './components/pages/ExercisePage'
import course from './components/pages/CoursPage'
import credits from './components/pages/Credits'
import profil from './components/pages/ProfilPage'
import admininistration from './components/pages/AdministrationPage'
import adminStats from './components/pages/Administration/StatsPage'
import adminExercises from './components/pages/Administration/ExercisesPage'
import adminUsers from './components/pages/Administration/UsersPage'
import aEDefault from './components/pages/Administration/ExercisesDefault'
import aEForm from './components/pages/Administration/AdministrationForm'
import aUDefault from './components/pages/Administration/UsersDefault'
import aUForm from './components/pages/Administration/UserForm'
import passwordForm from './components/pages/Administration/passwordForm'

Vue.use(Router);

function isLoggedIn() {
    return store.getters['user/isLoggedIn'];
}

function isAdmin() {
    return store.getters['user/isAdmin'];
}

export default new Router({
    linkActiveClass: 'is-active',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: '/exercise/:exerciseId',
            name: 'exercise',
            component: exercise,
            props: (route) => ({...route.params}),
            beforeEnter(to, from, next) {
                if (!isLoggedIn() || to.params.exercise == null) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: '/course/:courseId',
            name: 'course',
            component: course,
            props: (route) => ({...route.params}),
            beforeEnter(to, from, next) {
                if (!isLoggedIn() || to.params.course == null) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: '/profil',
            name: 'profil',
            component: profil,
            props: (route) => ({...route.params}),
            beforeEnter(to, from, next) {
                if (!isLoggedIn()) {
                    next('/');
                } else {
                    next();
                }
            }
        },
        {
            path: '/administration',
            component: admininistration,
            beforeEnter(to, from, next) {
                if (!isLoggedIn() || !isAdmin()) {
                    next('/');
                } else {
                    next();
                }
            },
            children: [
                {
                    path:'',
                    name:'adminDefault',
                    beforeEnter(to, from, next) {
                        next('/administration/stats');
                    }
                },
                {
                    path:'stats',
                    name: 'adminStats',
                    component: adminStats
                },

                {
                    path: 'exercises',
                    component: adminExercises,
                    children :[
                        {
                            path: '',
                            name:'adminExercises',
                            component:aEDefault
                        },
                        {
                            path: 'form',
                            name:'aEForm',
                            component:aEForm,
                            props: (route) => ({...route.params})
                        }
                        
                    ]
                    
                },

                {
                    path: 'users',
                    component: adminUsers,
                    children:[
                        {
                            path: '',
                            name:'adminUsers',
                            component:aUDefault
                        },
                        {
                            path: 'form',
                            name:'aUForm',
                            component:aUForm,
                            props: (route) => ({...route.params})
                        },
                        {
                            path: 'reset',
                            name: 'passwordForm',
                            component:passwordForm,
                            props: (route) => ({...route.params}),
                            beforeEnter(to, from, next) {
                                if (to.params.user == null) {
                                    next('/administration/users/');
                                } else {
                                    next();
                                }
                            }
                        }
                    ]
                    
                }
            ]
        },
        {
            path: `/bg`,
            name: 'credits',
            component: credits
        },
        {
            path: `*`,
            name: '404',
            component: pageNotFound
        }
    ]
});
