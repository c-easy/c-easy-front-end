import Vue from 'vue'

export default {
    getAllChapters() {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.get('chapter').then(result => {
                let output = {
                    success: result.data.success,
                    chapters: result.data.chapters
                };
                resolve(output);
            }).catch(error => {
                reject(error.response.data)
            });
        });
    },
    addChapter(chapter) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.post('chapter/add', { chapter: chapter }).then(result => {
                let output = {
                    success: result.data.success,
                    chapter: result.data.chapter
                };
                resolve(output);
            }).catch(error => {
                console.log(error.response.data)
                reject(error.response.data);
            });
        });
    },
    updateChapter(chapter) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.put('chapter/update', { chapter: chapter }).then(result => {
                let output = {
                    success: result.data.success,
                    chapter: result.data.chapter
                };
                resolve(output);
            }).catch(error => {
                console.log(error.response.data)
                reject(error.response.data);
            });
        });
    },
    deleteChapter(chapter) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.delete('chapter/delete', { data: {
                chapter: chapter 
            }}).then(result => {
                let output = {
                    success: result.data.success,
                    message: result.data.message
                };
                resolve(output);
            }).catch(error => {
                reject(error.response.data);
            });
        });
    }
}