import Vue from 'vue'

export function getAllPromos() {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.get('user/getAllPromos').then(result => {
            let output = {
                success: result.data.success,
                promos: result.data.promos
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function getAllUsersFromPromo(promo) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('user/getAllUsersFromPromo', { promo: promo }).then(result => {
            let output = {
                success: result.data.success,
                users: result.data.users
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function getUser(user) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('user/getUser', { user: user }).then(result => {
            let output = {
                success: result.data.success,
                user: result.data.user
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function createUser(newUser) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('user/create', { newUser: newUser }).then(result => {
            let output = {
                success: result.data.success,
                user: result.data.user
            };
            resolve(output);
        }).catch(error => {
            if(error.response.status == 423){
                error.response.data = {success: false, message: "Un utilisateur possède déjà l'identifiant rentré."}
            }
            if(error.response.status == 422){
                error.response.data = {success: false, message: "L'identifiant est trop long."}
            }
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function updateUser(oldUser) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.put('user/update', { oldUser: oldUser }).then(result => {
            let output = {
                success: result.data.success,
                user: result.data.user
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function deleteUser(oldUser) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.delete('user/delete', { data: {
            oldUser: oldUser 
        }}).then(result => {
            let output = {
                success: result.data.success,
                message: result.data.message
            };
            resolve(output);
        }).catch(error => {
            reject(error.response.data);
        });
    });
}

export function deletePromo(promo) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.delete('user/deletePromo', { data: {
            promo: promo 
        }}).then(result => {
            let output = {
                success: result.data.success,
                message: result.data.message
            };
            resolve(output);
        }).catch(error => {
            reject(error.response.data);
        });
    });
}