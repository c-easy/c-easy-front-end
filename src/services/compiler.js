import Vue from 'vue'

export function runTest(exercise) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('compile/runTest', { exercise: exercise }).then(result => {
            let output = {
                success: result.data.success,
                compiled: result.data.compiled
            };
            if (result.data.compiled) {
                output.pass = result.data.pass;
                output.testSet = result.data.testSet;
                resolve(output);
            } else if (result.data.compiled == false) {
                output.error = result.data.error;
                resolve(output);
            }
        }).catch(error => {
            reject(error.response.data);
        });
    });
}


export function compileAndTest(source, exercise) {
    return new Promise((resolve, reject) => {
        if (source == "") {
            reject({
                success: false,
                error: "Tu ne peux pas compiler du vide, écris quelque chose avant de réessayer."
            })
        }
        Vue.prototype.$http.post('compile/compileAndTest', { source: source, exercise: exercise }).then(result => {
            let output = {
                success: result.data.success,
                compiled: result.data.compiled,
                do: result.data.do
            };
            if (result.data.compiled) {
                output.pass = result.data.pass;
                output.testSet = result.data.testSet;
                resolve(output);
            } else if (result.data.compiled == false) {
                output.error = result.data.error;
                resolve(output);
            }
        }).catch(error => {
            reject(error.response.data);
        });
    });
}
