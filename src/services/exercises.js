import Vue from 'vue'

export default {
    getAllExercises() {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.get('exercise').then(result => {
                let output = {
                    success: result.data.success,
                    exercises: result.data.exercises
                };
                resolve(output);
            }).catch(error => {
                reject(error.response.data)
            });
        });
    },
    addExercise(exercise) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.post('exercise/add', { exercise: exercise }).then(result => {
                let output = {
                    success: result.data.success,
                    exercise: result.data.exercise
                };
                resolve(output);
            }).catch(error => {
                console.log(error.response.data)
                reject(error.response.data);
            });
        });
    },
    updateExercise(exercise) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.put('exercise/update', { exercise: exercise }).then(result => {
                let output = {
                    success: result.data.success,
                    exercise: result.data.exercise
                };
                resolve(output);
            }).catch(error => {
                console.log(error.response.data)
                reject(error.response.data);
            });
        });
    },
    deleteExercise(exercise) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.delete('exercise/delete', { data: {
                exercise: exercise 
            }}).then(result => {
                let output = {
                    success: result.data.success,
                    message: result.data.message
                };
                resolve(output);
            }).catch(error => {
                reject(error.response.data);
            });
        });
    }
}