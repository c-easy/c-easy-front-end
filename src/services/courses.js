import Vue from 'vue'

export default {
    addCourse(course, file) {
        let formData = new FormData();
        formData.append('pdffile', file);

        return new Promise((resolve, reject) => {
            Vue.prototype.$http.post('course/upload', formData, { headers: { 'Content-Type': 'multipart/form-data'  }}).then(result => {
                course.content =  process.env.VUE_APP_BACKEND_URL.concat(result.data.file);
                Vue.prototype.$http.post('course/add', {course: course}).then(result => {
                    let output = {
                        success: result.data.success,
                        course: result.data.course
                    };
                    resolve(output);
                }).catch(error => {
                    console.log(error.response.data)
                    reject(error.response.data);
                });
            });     
        });
    },

    updateCourse(course, isContentUpdated, file) {
        if(isContentUpdated){
            let formData = new FormData();
            formData.append('pdffile', file);
            return new Promise((resolve, reject) => {
                Vue.prototype.$http.post('course/upload', formData, { headers: { 'Content-Type': 'multipart/form-data'  }}).then(result => {
                    course.newContent = process.env.VUE_APP_BACKEND_URL.concat(result.data.file);
                    Vue.prototype.$http.put('course/update', {course: course}).then(result => {
                        let output = {
                            success: result.data.success,
                            course: result.data.course
                        };
                        resolve(output);
                    }).catch(error => {
                        console.log(error.response.data)
                        reject(error.response.data);
                    });
                });
            });
        }
        else {
            return new Promise((resolve, reject) => {
                Vue.prototype.$http.put('course/update', { course: course }).then(result => {
                    let output = {
                        success: result.data.success,
                        course: result.data.course
                    };
                    resolve(output);
                }).catch(error => {
                    console.log(error.response.data)
                    reject(error.response.data);
                });
            });
        }
    },
    deleteCourse(course) {
        return new Promise((resolve, reject) => {
            Vue.prototype.$http.delete('course/delete', { data: {
                course: course 
            }}).then(result => {
                let output = {
                    success: result.data.success,
                    message: result.data.message
                };
                resolve(output);
            }).catch(error => {
                reject(error.response.data);
            });
        });
    }
}