import Vue from 'vue'

export function getAllDosFromUser(user) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/getAllDosFromUser', { user: user }).then(result => {
            let output = {
                success: result.data.success,
                dos: result.data.dos
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function getAllDosFromPromo(promo) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/getAllDosFromPromo', { promo: promo }).then(result => {
            let output = {
                success: result.data.success,
                dos: result.data.dos
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}


export function getAllDos() {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/getAllDos').then(result => {
            let output = {
                success: result.data.success,
                dos: result.data.dos
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function addDo(exercise) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/add', { exercise: exercise }).then(result => {
            let output = {
                success: result.data.success,
                do: result.data.do
            };
            resolve(output);
        }).catch(error => {
            reject(error.response.data);
        });
    });
}

export function getDoFromExercise(exercise) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/getDoFromExercise', { exercise: exercise }).then(result => {
            let output = {
                success: result.data.success,
                do: result.data.do
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}

export function getDoFromUserFromExercise(user, exercise) {
    return new Promise((resolve, reject) => {
        Vue.prototype.$http.post('do/getDoFromUserFromExercise', { user: user, exercise: exercise }).then(result => {
            let output = {
                success: result.data.success,
                do: result.data.do
            };
            resolve(output);
        }).catch(error => {
            console.log(error.response.data)
            reject(error.response.data);
        });
    });
}