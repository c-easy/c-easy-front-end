
export function visible (Objects, that) {
    return Objects.filter(function (object) {
        
        if (object.order != -1){

            if(object.courses != null){
                if(that.visible(object.courses, that).length > 0)
                    return object;
                else
                    return null;
            }

            if(object.exercises != null){
                if(that.visible(object.exercises ,that).length > 0)
                    return object;
                else
                    return null;
            }

            return object;
        }
        return null;
    });
}