import axios from 'axios'

let api = axios.create({
    baseURL: process.env.VUE_APP_BACKEND_URL + "api/",
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});

export default {
    initializeAxios(){
        return api
    },

    setAuthorizationHeaders(token){
        api.defaults.headers['Authorization'] = `Bearer ${token}`
    },

    deleteAuthorizationHeaders(){
        delete api.defaults.headers['Authorization'];
    }
} 